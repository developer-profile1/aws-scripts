#/bin/bash
#
# Creating static website in s3 bucket
echo "Endpoint  https://momofun.s3.eu-central-1.amazonaws.com:443"
aws s3 mb s3://momofun/
read -p "Press any key to continue.."
aws s3 website s3://momofun/ --index-document index.html --error-document error.html $1